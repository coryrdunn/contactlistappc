//
//  UpdateViewController.m
//  ContactListApp
//
//  Created by Cory Dunn on 11/4/16.
//  Copyright © 2016 Cory Dunn. All rights reserved.
//

#import "UpdateViewController.h"

@interface UpdateViewController ()

@end

@implementation UpdateViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(editContact:) name:@"editContact" object:nil];
}

- (void)editContact:(NSNotification*) note {
    NSManagedObject *details = note.object;
    
    self.nameTxtField.text = [details valueForKey:@"name"];
    self.phoneTxtField.text = [details valueForKey:@"phone"];
    self.emailTxtField.text = [details valueForKey:@"email"];
    self.addressTxtField.text = [details valueForKey:@"address"];
}


- (IBAction)cancelBtnTouched:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)saveBtnTouched:(id)sender {
    AppDelegate *ad = [[UIApplication sharedApplication]delegate];
    NSManagedObjectContext *context = ad.persistentContainer.viewContext;
//    NSLog(@"stop");
    NSManagedObject *newContact = [NSEntityDescription insertNewObjectForEntityForName:@"Contact" inManagedObjectContext:context];
    [newContact setValue:self.nameTxtField.text forKey:@"name"];
    [newContact setValue:self.phoneTxtField.text forKey:@"phone"];
    [newContact setValue:self.emailTxtField.text forKey:@"email"];
    [newContact setValue:self.addressTxtField.text forKey:@"address"];
    
    NSLog(@"stop");
    
    NSError *error = nil;
    if(![context save:&error]) {
        NSLog(@"Save Incomplete");
    }
    
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
