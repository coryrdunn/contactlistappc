//
//  DetailsViewController.h
//  ContactListApp
//
//  Created by Cory Dunn on 11/4/16.
//  Copyright © 2016 Cory Dunn. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>
#import "UpdateViewController.h"

@interface DetailsViewController : UIViewController
@property (strong, nonatomic) IBOutlet UITextView *emailTxtView;
@property (strong, nonatomic) IBOutlet UITextView *phoneTxtView;
@property (strong, nonatomic) IBOutlet UITextView *addressTxtView;
@property (strong, nonatomic) IBOutlet UIButton *directionsBtn;
@property (strong, nonatomic) IBOutlet MKMapView *mapView;
@property (nonatomic, strong) NSManagedObject* details;

@end
