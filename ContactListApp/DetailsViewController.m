//
//  DetailsViewController.m
//  ContactListApp
//
//  Created by Cory Dunn on 11/4/16.
//  Copyright © 2016 Cory Dunn. All rights reserved.
//

#import "DetailsViewController.h"

@interface DetailsViewController ()

@end

@implementation DetailsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

-(void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    self.title = [_details valueForKey:@"name"];
    self.phoneTxtView.text = [_details valueForKey:@"phone"];
    self.emailTxtView.text = [_details valueForKey:@"email"];
    self.addressTxtView.text = [_details valueForKey:@"address"];
    
    CLGeocoder *geoCoder = [[CLGeocoder alloc]init];
    NSString *address = [_details valueForKey:@"address"];
    [geoCoder geocodeAddressString:address completionHandler:^(NSArray *placemarks, NSError *error) {
        if(error) {
            NSLog(@"Error");
        }
        if(placemarks && placemarks.count > 0) {
            CLPlacemark *placemark = placemarks[0];
            CLLocation *location = placemark.location;
            CLLocationDistance radius = 1000;
            CLLocationCoordinate2D loc = CLLocationCoordinate2DMake(location.coordinate.latitude, location.coordinate.longitude);
            MKCoordinateRegion coordRegion = MKCoordinateRegionMakeWithDistance(loc, radius, radius);
            self.mapView.region = coordRegion;
        }
    }];
}

- (IBAction)editBtnTouched:(id)sender {
    [[NSNotificationCenter defaultCenter] postNotificationName:@"editContact" object:_details];
    
    [self performSegueWithIdentifier:@"editBtnTouched" sender: sender];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
