//
//  main.m
//  ContactListApp
//
//  Created by Cory Dunn on 11/2/16.
//  Copyright © 2016 Cory Dunn. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
