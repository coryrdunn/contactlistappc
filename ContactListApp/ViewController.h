//
//  ViewController.h
//  ContactListApp
//
//  Created by Cory Dunn on 11/2/16.
//  Copyright © 2016 Cory Dunn. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreData/CoreData.h>
#import "DetailsViewController.h"
#import "AppDelegate.h"

@interface ViewController : UIViewController<UITableViewDataSource, UITableViewDelegate>
{
    NSMutableArray* contacts;
}

@property (strong, nonatomic) IBOutlet UITableView *tableView;
@property (strong, readonly) NSManagedObjectContext *viewContext;
@property (strong, readonly) NSManagedObjectContext *parentContext;



@end

