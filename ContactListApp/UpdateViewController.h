//
//  UpdateViewController.h
//  ContactListApp
//
//  Created by Cory Dunn on 11/4/16.
//  Copyright © 2016 Cory Dunn. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreData/CoreData.h>
#import "AppDelegate.h"
#import "ViewController.h"

@interface UpdateViewController : UIViewController
@property (strong, nonatomic) IBOutlet UITextField *nameTxtField;
@property (strong, nonatomic) IBOutlet UITextField *phoneTxtField;
@property (strong, nonatomic) IBOutlet UITextField *emailTxtField;
@property (strong, nonatomic) IBOutlet UITextField *addressTxtField;
@property (nonatomic, strong) NSDictionary* details;
@property (strong, readonly) NSManagedObjectContext *viewContext;


@end
